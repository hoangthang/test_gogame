const defaultConfig = {
	language: "vi",
	availableLanguage: ["vi", "en"],
	apiURL: "https://gosale-c6ba7.firebaseapp.com/api",
	secretToken: '^*B(H()',
	brandName: "GoGame",
	options: [{ title: 'A', color: '#FF5254' }, { title: 'B', color: '#00BDF9' }, { title: 'C', color: '#FFD21E' }, { title: 'D', color: '#43C687' }],
	// server: "wss://server.gogame.run:2567"
	server: "wss://backend.gogame.run:2567"

}

module.exports = defaultConfig
