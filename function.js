let Colyseus = require("colyseus.js");
let config = require("./config/index");

class Bot {
  constructor(name, pin) {
    this.pin = pin;
    this.name = name;
    this.roomId = null;
    this.connection = null;
    this.state = null;
    this.ansers_test = [0, 1, 2, 3];
    this.time_arr = [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
      32,
      33,
      34,
      35
    ];
    this.time_out = [];
    this.amount_question = 0;
  }

  joinLobby() {
    for (let i = 0; i < 1000; i++) {
      this.time_out.push(i);
    }
    this.connection = new Colyseus.Client(config.server);
    // console.log(this.pin);
    this.connection
      .joinOrCreate("lobby", { room: this.pin.toString() })
      .then(room => {
        room.onMessage(data => {
          switch (data.action) {
            case "room":
              this.roomId = data.roomInfo.id;
              room.leave();
              break;
            case "error":
              console.log(this.name, "Bot error");
              // this.error = this.$t("error." + data.error);
              room.leave();
              break;
          }
        });
        room.onLeave(code => {
          if (this.roomId) this.joinGame();
        });
      });
  }
  joinGame() {
    let data = { room: this.pin, name: this.name };
    console.log(this.name, "join game");
    this.connection
      .joinById(this.roomId, data)
      .then(room => {
        this.configRoom(room);
      })
      .catch(e => {
        console.log("join failed", this.name);
        if (e.toString().indexOf("no rooms found") > 0) {
          this.error = this.$t("game.no_room_found");
        } else {
          this.error = e.toString();
        }
      });
  }
  configRoom(room) {
    // console.log(this.time_out,"Data");
    // setTimeout(() => {
    //   room.leave();
    //   console.log(this.name, "Leave room in question", this.amount_question);
    // }, this.time_out[Math.floor(Math.random() * this.time_out.length)] * 1000);
    // this.clientId = room.sessionId;

    room.onStateChange(state => {
      // console.log(state.state);
      if (state.state == "answering") {
        setTimeout(() => {
          let answer = this.ansers_test[
            Math.floor(Math.random() * this.ansers_test.length)
          ];
          room.send({ action: "answer", answer });
        }, this.time_arr[Math.floor(Math.random() * this.time_arr.length)] * 1000);
      } else if (state.state == "ended") {
        room.leave();
      } else if (state.state == "reading_question") {
        this.amount_question++;
      } else if (state.state == "waiting_start") {
       let data = {
          email:
            this.time_out[Math.floor(Math.random() * this.time_out.length)] +
            `@gmail.com`,
          phone:
            this.time_out[Math.floor(Math.random() * this.time_out.length)] +
            `78910`
        };
        room.send({ action: "personalInfo", data });
      }
    });
  }
}
module.exports = Bot;
